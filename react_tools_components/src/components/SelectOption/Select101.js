//Bootstrap Package Used Component

import React,{useState} from "react"
// import './Select101.css';

function Select101() {
  const [foodState, setFoodState] = useState("")

  return (
    <div className="container pt-5">
      <select 
        className="form-select"
        onChange={(e) => {
          const selectedFood = e.target.value;
          setFoodState(selectedFood)
        }}
      >
        <option value="steak">Steak</option>
        <option value="sandwich">SandWich</option>
        <option value="dumpling">Dumpling</option>
      </select>

      {/* Now selected option value can be accessed in food state  */}
      {foodState}

    </div>
  );
}

export default Select101;
